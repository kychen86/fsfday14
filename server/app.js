var express = require("express");
var path = require("path");
var bodyParser = require('body-parser');


const NODE_PORT = process.env.NODE_PORT || 8080;
const CLIENT_FOLDER = path.join(__dirname , '/../client');
const MSG_FOLDER = path.join(CLIENT_FOLDER, '/assets/messages');

var app = express();

//STATIC CONTENT
app.use(express.static(CLIENT_FOLDER));


//Error Messages
//Handles resource not found
app.use(function(req, res) { //Use as a catch to answer when all else fails, must be placed right at the bottom of the list
  res
    .status(404)
    .sendFile(path.join(MSG_FOLDER, '/404.html'));
});

// Handles server error
app.use(function(err, req, res, next) { // notice error in 500s hold 4 parameters.
  console.log(err);
  res
    .status(500)
    .sendFile(path.join(MSG_FOLDER, "/500.html"));
});

//Start Server
app.listen(NODE_PORT, function(){
    console.log("Welcom to port: " + NODE_PORT)
}) 